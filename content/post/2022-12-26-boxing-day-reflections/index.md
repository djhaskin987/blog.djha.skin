+++
title = "Boxing Day Reflections"
date = 2022-12-26
tags = ["holiday-specials","pandemic"]
categories = ["HolidaySpecials"]
+++


```
Started writing this 2022-12-13T17:22:02-0700
```

## 2022: Healing from the Pandemic

As I look on this last year, I have decided it was one of healing. Healing from
the pandemic, mostly. I had a job that was hard on me because I crave social
experiences. This particular job was a fine employer, but they were more
introverted. Social interaction was less of a necessity.

## 2020-2021: The Pandemic and its Impact on Me

When the pandemic hit, everyone was ordered home and we stayed there.

As if this wasn't difficult enough, I was in a department that was associated
with on-premise virtual machines, which had recently gone out of vogue at the
company. Everyone was moving to cloud.

This was also fine -- I am all for the cloud as much as the next engineer.
However, it caused my team to be associated with the stigma of on-premise. This
basically meant that people stopped asking us for favors. Work dried right up.
Near the end, I was doing nothing of value at work, and they didn't notice or
care. The lack of work, coupled with the lack of interaction, left me deeply
anxious and depressed. I was showing signs of burnout, but it was strange
because I wasn't really working on anything.

## Weathering the Storm with Knitting and Other Things

I got therapy, I got on new drugs, got into a good excercise routine -- the
whole nine yards. But try as I might, I just couldn't get to a place where I
was happy. During this time, I started a hobby that I had learned from my Mom
as a kid. I took up knitting. It sounds strange, but there's something incredibly
soothing about the repetitive motions of knitting, and at the end of a stretch
of time, it's fun to see the brand new cloth (and the pattern it contains) that
didn't used to be there before.

I didn't realize how much this problem was tied to my then-current
employer (plus the pandemic) until I couldn't take it anymore and switched
jobs near the end of 2021. Being more of a start-up, the new job allowed us to
come into the office, even encouraging it. Also because of its start-up nature,
I had *plenty* of work to do. So many of my anxiety and depression problems
disappeared. I knew I had really healed up when I found knitting boring.

## Holding on to the Hobby

I can still knit if I'm listening to an audio book, it's still fun, even.

The reason I thought of all this was because this is a Boxing Day post, and I
had committed to knitting 5 hours every week from September of 2022 until
Christmas 2022. I did pretty good at it until I got sick for a few weeks at the
end of November this year. I need to get back at it, but my goals of actually
finishing that sweater for my mother-in-law and wife by Christmas seem a
stretch at this point.

Still, I committed to it also because it gave me so much comfort and joy, so
much peace during a time when I couldn't remember what peaceful felt like. When
the jitters took me and didn't let me go. And it's still fun, the art of
creation. I will recommit to it. I don't want to let it die.

## Why it Matters to Programmers

Kind of funny, discussing knitting on a programming blog. However, I think
there's still a moral here. Creative hobbies that create real things in the
real world can really help balance and rejuvenate programmers, we who spend all
our energies making [pretty lights flash on a screen](https://xkcd.com/722/),
who change lines of code but not anything really physical, who type with our
hands but do not work with our hands. We see this everywhere, most notably when
engineers [burn out so much from
programming](https://twitter.com/destroytoday/status/1385019337220759555) that
they [no longer build
software](https://github.com/docker/cli/issues/267#issuecomment-695149477),
dedicating all their time to more tangible pursuits. Dedicating some time to
such a hobby *while* programming, I have found, really helped me avoid burnout
during a time when I felt like I had fell out of the frying pan and into the
fire, burnout-wise. Keeping hobbies like this can keep us sane.

For any knitters reading this, I'm [scruffmcgruff on
Ravelry](https://www.ravelry.com/people/scruffmcgruff). Cheers!