+++
title = "Going Self-Hosted with Git"
date = 2022-11-09
tags = ["tooling"]
+++

I started working out of [SourceHut](https://sr.ht/) after GitHub used everyone's code to train [an AI that flagrantly violates copyright right and left](https://matthewbutterick.com/chron/this-copilot-is-stupid-and-wants-to-kill-me.html). Then [SourceHut said that they wouldn't let anyone on their platform who was building their project around blockchain](https://sourcehut.org/blog/2022-10-31-tos-update-cryptocurrency/). [I disagreed with their decision](https://news.ycombinator.com/item?id=33405578), so I'm moving to a self-hosted solution.

I'll be updating links to my repos in past posts in this blog. Hopefully since I'm self-hosting this will be the last time :)

I've looked around a lot. [Savannah](https://savannah.gnu.org/), [Gitea](https://gitea.io/en-us/), [RocketGit](https://rocketgit.com/), and [Pagure](https://pagure.io/) were investigated. I decided on Gitea, since:

  - It has minimal system requirements (unlike Pagure/Savannah)
  - The community around gitea is explicitly motivated to make the software, not make money off of some hosted version of it. This means the project is likely to thrive and listen to people who host it, instead of the their own hosting needs.
  - They have SMTP authentication, which is just perfect for me since I already set up my own email (powered by [Migadu](https://migadu.com/public/login) -- [recommended by Drew DeVault](https://drewdevault.com/2020/06/19/Mail-service-provider-recommendations.html), ironically).

So yeah. Wish me luck. Those new links are up.

Cheers :)