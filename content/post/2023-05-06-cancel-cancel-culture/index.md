+++
title = "Cancel Cancel Culture"
date = 2023-05-06
tags = []
categories = []
+++

2023-05-06T09:14:17

I was reading a now deleted thread from Hacker News about how a certain
individual who had contributed a great deal to modern computing had expressed
morally alarming opinions and/or done some horrible act on the internet. These
trespasses were found to be so egregious that many people objected to some of
his contributions being attributed to him. They wished to use his
accomplishments while forgetting to whom they were indebted for the use of them.
I omit his name here because it's not important. It's also good to be vague
because several people match this description, and my argument applies to all of
them.

"Those who live in glass houses should not throw stones".  Never has there been
a time where our lives were more transparent, our reputations more fragile, then
this time of the internet. Information about our lives going back years stands
readily available to anyone wishing to search it out. AIs
remember everything that they have read (which corpus often spans _the entire
internet_) and are able to recall that information with a high degree of
accuracy. We truly live in glass houses in this day and age.

Whenever I read about how people want to throw out the good deeds of their
forebearers with the bad, I am saddened because people who try to cancel
others never acknowledge the good deeds or humanity of those whom they seek to
cancel. Always with them the baby is thrown out with the bathwater.

While thinking about this subject, I often think of a passage in one of my
favorite novels. It comes from the novel quote [The Black Cauldron][1] by Lloyd
Alexander. In the novel, a character known as [King Morgant][2] betrays the
protagonist. The protagonistic band are finally defeat and kill him, largely due
to the sacrifice of another character known as [Ellidyr][3].

Ellidyr longed to build a legacy for himself, but never could. He even
tried to kill the main characters in his quest to leave a legacy. Once he
realized that legacy wasn't as important as doing the right thing, he sacrificed
himself to save others. Ironically, this became his legacy.

Conversely, Morgant had already left a strong legacy of loyalty and great deeds,
but turned from it at the last in favor of seeking power.

Ultimately, Prince Gwydion, the author's insert character, sets up memorials for
_both_.

> "Morgant? Taran asked, turning a puzzled glance to Gwydion. "How can there
> be honor for such a man?"
>
> "It is easy to judge evil unmixed", replied Gwydion. "But, alas, in most of
> us good and bad are closely woven we the threads on a loom; greater wisdom
> than mine is needed for the judging."
>
> "King Morgant served the Sons of Don long and well," he went on. "Until the
> thirst for power parched his throat, he was a fierce and noble lord. In
> battle he saved my life more than once. These things are part of him and
> cannot be put aside nor forgotten.
>
> "And so I shall honor Morgant," Gwydion said, "for what he used to be, and
> Ellidyr Prince of Pen-Llarcau for what he became."

We can learn a lot from this passage. We can and should honor those who have
contributed to our lives, even though they do other things later in their life
with which we disagree. It is important to acknowledge people for all the good
deeds that they did and do, even if they go astray. This encourages onlookers to
do good deeds as well. We should not forget the giants whose shoulders we have
built on, even if they later disappoint us. If we do, on what perch do we have
to stand?

[1]: https://en.wikipedia.org/wiki/The_Black_Cauldron_(novel)
[2]: https://prydain.fandom.com/wiki/King_Morgant
[3]: https://prydain.fandom.com/wiki/Prince_Ellidyr