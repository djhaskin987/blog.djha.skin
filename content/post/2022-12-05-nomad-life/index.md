+++
title = "Nomad Coder: Programming Wherever"
date = 2022-12-05
tags = ["tooling"]
categories = ["Tooling"]
+++

```
2022-11-11T08:01:33-0700
```

I am typing this right now in I-can-see-my-breath weather outside with glare on
the screen coming from the brilliant morning sun. I love being able to do that.

I used to bike a lot before I got long covid. In any case, I still use the
train and bus a lot. I like being able to code wherever I am. Having done this
for around 13 years, I have picked up some tricks along the way for how to do
this.

## Tips I Strongly Recommend

When you want to be able to code anywhere:

1. **Wear fingerless gloves** (during cold weather). My mom gave me a pair of
   [MukLuks]() 10 years ago for Christmas and they changed my life. fingerless
   gloves make warm without getting in the way of the typer. I am currently
   wearing a version of those that I hand-knitted. (I'm `scruffmcgruff` on
   Ravelry if you are interested in looking up my knitting projects). They are
   a bit loose, so under them I am wearing a $1 pair of very thin gloves I
   found at the grocery store. They are so thin that I can use a touch screen
   with them and feel the bumps on the "F" and "J" keys. I can type through
   them. So my hands are nice and toasty while typing.
2. **Laptop: compact, bright screen, long battery.**  When I bought an [HP
   Spectre x360]() in 2019, it had nearly the brightest screen (if not THE
   brightest) on the market. It had a 1080p monitor, which was not 4k, but it
   bought me an 8 hour battery life. Very handy for a nomad. Now if I can only
   get it fedora to [sleep, then hibernate on it]() (tricky, I'm still having
   problems), I'll be even happier. That way, I can just shut the lid when I've
   reached my destination. If I have extra time because I got on the bus, it
   will just come back from sleep, while if I have already arrived, it will
   dutifully hibernate to save battery.
3. **Light-colored themes**. When you have full afternoon sun on your laptop
   screen, it may seem impossible to see what you are working on. It definitely
   is if you use "night" theme everywhere, let me tell you. But if you use a
   bright theme on your terminal, you can have the sun at your back and still
   see what you are doing, even when the glare is seemingly orders of magnitude
   brighter than the screen itself. I don't know why; perhaps something to do
   with how LCD screens work. I recommend [Solarized Light](), it's a theme
   that's defined for every IDE and terminal or editor ever. That's why I use
   it. I also like Solarized because it allows you to switch back and forth
   between a light and dark theme. I switch to a dark theme when I'm coding in
   the passenger's seat of a car on a road trip and it's dark so I don't want
   to disturb the driver with my bright screen.
4. **Download Files Beforehand**. This applies to Spotify playlists, audible
   books, podcasts, and other things to listen to. It also applies to
   documentation, If you work in Common Lisp for example, download the
   HyperSpec to your laptop. Having it downloaded helps with spotty
   connections.

That's all the tips I can think of for now.

## Other Things I Do and You Can, Too

* I wear a hoodie everywhere. I can pull the sleeves through the handle of my
  backpack if I get too hot, and they have built-in protection for my head and
  neck, but still they are still light enough to wear inside if I get a little
  cold.
  ![My hoodie when stored on the outside of my bag.]()
* I listen to a lot of podcasts during my commute. I also read my scriptures and
  write in my journal.

## Nomad Life is Freedom

When you are travelling, people leave you alone. No one asks for favors from
you because you are already using your time to travel. It is peaceful and
largely undisturbed. A little preparation goes a long way in terms of comfort.
I highly recommend coding while traveling.