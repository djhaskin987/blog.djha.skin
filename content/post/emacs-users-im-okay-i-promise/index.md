+++
title = "Emacs Users: I'm Okay, I Promise"
date = 2022-10-10
categories = ["Tooling"]
tags = ["rants","vim","emacs"]
+++

![I'm a Vim User.](7cb6457f.png)

I wanted to put a few comments down about what people should understand about
my switching from Emacs to Vim. I often get into arguments with people online
about this issue. Most recently this came up when I discussed my [Vim set-up for developing Common Lisp](https://blog.djha.skin/blog/developing-common-lisp-using-gnu-screen-rlwrap-and-vim/). I thought that putting something down in my blog might help
me settle my opinion for others to read in full so that I can save time by
giving them the link to this article next time.

I learned Vim on my own as a child. When my Dad told me he used it in college,
I wanted to learn what he knew. I used it up until about 2012, 3 years or so
into my professional computing career.

A lot of my friends knew Emacs, so I decided to learn it. I learned it. I was
very fast in it. I did a lot of good in it. [Magit](https://magit.vc/),
[eshell](https://www.gnu.org/software/emacs/manual/html_mono/eshell.html),
[SLIME](https://slime.common-lisp.dev/), [Org Mode](https://orgmode.org/), the
whole nine yards. Then and around 2015 I started to get repetitive strain
injury (despite having [remapped the capslock
key](https://www.emacswiki.org/emacs/MovingTheCtrlKey)). It took me years to
fix the injury, using [stretches I found
online](https://www.redboxfitness.com/forearm-stretches/) and ditching [my
mechanical keyboard](https://matias.ca/ergopro/pc/) in favor of [rubber top
keys](https://www.microsoft.com/en-us/d/microsoft-ergonomic-keyboard/93841ngdwr1h?activetab=pivot:overviewtab).
I found however that no matter how hard I tried to go back, I had to ditch
Emacs if my injuries were to improve. I already knew Vim, so it was mostly easy
to go back. I did have to reorient some of my tools; I missed org mode the
most. Later on, I did get my workflow back up to scratch with markdown using
some interesting keybindings.

At this point in the story Emacs users come out of the woodwork to suggest
things that could help me come back to Emacs. Remapping capslock key (I already
did), don't forget right-control (trying), use
[Evil](https://github.com/emacs-evil/evil) (eh, I already know vim pretty
well). They assume that I am "not okay", that I would be so much happier if I
could go back. They believe that Emacs is better than Vim.

I don't blame them for this. Of course they think it's better, or they wouldn't
have chosen it. I should know, I was an Emacs user. Indeed, because of Org mode,
I did in fact try very hard to go back for a while. I can vouch for it being a
fantastic tool set. But what these users need to understand is that *Vim
toolsets and workflows are also fantastic*.

Emacs users often try to use Vim and end up going back to Emacs because it
doesn't fit their needs. Again, this is fine. But I think that maybe it might
fit their needs if they understood that using Vim is not like using Emacs.
There is a huge communication disconnect between the two communities. This is
simply because, at their core, they are entirely different tools.

Emacs is designed to be the extensible editor. When you need more than just an
editor for your job, Emacs becomes more than an editor to match. Vim is
designed to do one thing and do it well: edit. To make up for this narrow focus
on functionality though, Vim is very good at playing nice with its neighbors
and hooking into other tools.

Perhaps the idea of starting from inside the editor and growing from there is
why Emacs is so popular with programmers. Like contemporary cars, Emacs started
out as a simple car, like the Model T Ford. It started just being an editor
like the model T was just a box with self-propelled wheels. As time went on
both the Ford and Emacs added capabilities: air conditioning and electric and
entertainment systems for the car, compilers and shells for Emacs. At the end
you end up with an RV: A vehicle that can do literally anything including have
a kitchen sink.

Vim is well designed to work inside other contexts. Like the evolution of the
car, it too grew, but only to a point. Instead of growing into an RV, Vim grew
into a pickup truck. It became something that you add to a larger ecosystem.
If you need an RV, you can put a camper in the bed or you can haul a fifth wheel behind you. The
flexibility and loose coupling with other tools that Vim demonstrates appeals
to Vim users.

Like pickup trucks, it is and always has been the chief tool in its class for
operators. Its ancestor,
[ed](https://pubs.opengroup.org/onlinepubs/9699919799.2008edition/utilities/ed.html),
wasn't just designed for people who edit code all day for Unix, but for
[everyone that used Unix in any
way](https://cs.wellesley.edu/~cs249/Resources/ed_is_the_standard_text_editor.html).
Because of this, it evolved to easily fit into whatever context its user was
already in.

To be most effective, Vim should be used from inside other tools. [Vim inside
IntelliJ](https://plugins.jetbrains.com/plugin/164-ideavim), [Vim inside VS
Code](https://github.com/vscode-neovim/vscode-neovim). My favorite parent
context is Vim inside the terminal multiplexor [GNU
Screen](https://www.gnu.org/software/screen/) (It's like `tmux`). Vim provides
editing capability to a wider field of tools. **Very often, Vim professionals
will first learn those wider set of tools and then add Vim on top**. This is
part of [the Zen of Vim](https://blog.sanctum.geek.nz/vim-koans/), its core
philosophy.

Not all of Vim users use Vim this way. Some like to extend the editor beyond
its boundaries as an editor like Emacs does.
[Fugitive](https://github.com/tpope/vim-fugitive) is a Vim plugin that makes
them a git client, for example, much like Emacs' Magit. I personally find it
faster to just use the command line. Whenever I use tools like this, I feel
like it makes Vim clunky, going against its grain.

However, when Vim plugins work to make Vim simply a better editor, I feel like
these plugins really help. [Vim-sexp](https://github.com/guns/vim-sexp) is one
such plug-in. It makes no assumptions about how you interact with a repl or
what dialect of lisp you're using. It does make Vim "aware" of s-expressions
though. This allows me to use whatever tools I want but still get the benefit
of my editor knowing about what precisely I am using it to edit.

Other plugins I prefer in Vim don't make Vim a better editor, but they do make
Vim a better neighbor. They teach it to play nice with other tools, which is
the other core tenet of Vim. The shining example of this for me is jpalardy's
[Vim-Slime](https://github.com/jpalardy/vim-slime). These plugins allow Vim to
interact with other tools in loosely coupled way.

Emacs users think that using their editor within the context of other tools is bad, and from
their perspective it would be. After all, you would have to learn all of those
other tools instead of just one editor. But this is is the same problem from
the perspective of those who learned Vim. Why learn all those extra key
bindings in SLIME when I already know all of them in GNU Screen? Why
learn SLIME when every key binding for SLIME I've ever seen is simply a short-cut for a
command line/repl command? Vim-Slime allows me to use Vim and capitalize on all that
GNU Screen knowledge I already have. If I already know the command line, SLIME
doesn't really add anything. It only really adds if I don't know the command
line yet. But I *do*. I *have* CLI muscle memory, lots of it.

Others like Vim because of situations like this: I already have to learn VS
Code because my team decided on it as a standard tool, but I want a modern
editing experience and *nice things* like `:%s/\<old_var\>/NewVar/g`, `:g/^
*#.*$/d`, and `:vimgrep 'problemcode' **/*.c`.

I have been an Emacs user and I really liked it. What they're doing is great.
But what I'm doing now is also great. Vim may not be the whole of what I need,
but it's *amazing* at editing. In whatever tool *you're already using*, if you
want a better editing experience, add Vim to it.

To be very clear then, Emacs users: Rock on. Be happy. But I am also happy. Vim
( + other tools ) is *fine*. It's a great way to code. I invite you to try it
sometime.