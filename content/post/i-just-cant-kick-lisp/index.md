+++
title = "I Just Can't Kick Lisp"
date = 2022-10-02
tags = ["common-lisp"]
categories = ["CommonLisp"]

+++

I recently read [mfiano's blog
post](https://mfiano.net/posts/2022-09-04-from-common-lisp-to-julia/) about
Common Lisp. He makes the point that the language attracts loners. To make your
project lisp is to ensure no one will want to work in it with you.

He's right. Not just for Common Lisp, either. But I just can't kick lisp. And
on the other side of that coin, if I think I will only ever develop a project
on a small team, the nimbleness of Lisp helps. I feel like Lisp is custom built
for [high functioning, high quality, low-head-count
teams](http://www.paulgraham.com/avg.html). I kinda feel like most of my
projects fall into this category.

At the end of the day, it's just more fun. And I am doing this stuff in my
spare time.