+++
title = "50MB for Hello World"
date = 2022-09-26
tags = ["common-lisp", "clojure", "devops"]
categories = ["CommonLisp"]
+++

2022-09-26T07:07:28-0600

Compiled languages are getting more important since Moore's law has stalled. We want more speed.

But really, as a DevOps engineer, I've learned to always value languages where it's easy to compile because they're easy to distribute. I don't have to make sure that an interpreter is installed on somebody else's machine to code with them. It's actually a huge barrier to entry for sharing. I myself have noticed that I don't like downloading npm cli modules because I don't program on Node enough to want to install Node, for example.

I've been investigating Common Lisp as a replacement for Clojure in my after-hours programming. My favorite type of application to program is small command line applications that make it easier to do DevOps work. Clojure's startup time makes this difficult. I should use golang, or rust or something, but I can't seem to kick lisp.

So I started learning Common Lisp and getting comfortable in the editor and the REPL. I've heard good things about how easy it is to compile CL. Then just to give it a whirl I compiled a hello world program (just prints out hello world).

Imagine my surprise when the compiled image Steel Bank spit out was 56 MB. That's a bit hefty for a small command line application.

(As an aside, I'm learning that Common Lisp and Clojure your both "expect" you to be making large, long running monolithic applications that make such egregious uses of time (Clojure start-up) and space (56MB) worth it in the long run. I'm actually in favor of monoliths but it doesn't fit my use case.)

Thinking about it this makes sense:

    The ANSI Common Lisp standard doesn’t mention this
    topic. (Keep in mind that it was written at a time where
    Lisp Machines were at their peak. On these boxes Lisp
    *was* your operating system!)

[Emphasis mine](https://lispcookbook.github.io/cl-cookbook/os.html). A language that used to be an entire operating system is probably difficult to get into a small tiny little executable. Still for my purposes this seems like the wrong tool.

I kept on trying. I tried with Clozure CL, golang, and C all with hello world. The table summarizes my results:

```
| Platform | Size  |
|----------|-------|
| SBCL     | 56MB  |
| Clozure  | 26MB  |
| Golang   | 1.9MB |
| C        | 17K   |
```

I was even surprised at golang. 2 MB for hello world! I guess we have to pay for our comforts in life. I'm not spending my after-hours riding C, I love the language and I know how to ride in it but I just don't have enough time to properly devote to it.

So where to go from here? There's [Chez Scheme](https://www.scheme.com/chez.html) and [Carp](https://github.com/carp-lang/Carp), but they don't have a lot of support. There's no Chez Scheme library for yaml for example. Racket has the same problem as common lisp, it bundles the kernel with the command line application you're making. Looking like we're going to start using golang more. Sorry Lisp. Hopefully we can get back together again someday.

Published 2022-09-26T07:39:01-0600