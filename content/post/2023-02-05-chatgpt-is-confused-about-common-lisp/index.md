+++
title = "ChatGPT is Confused about Common Lisp"
date = 2023-02-05
tags = ["chatgpt","common-lisp"]
categories = []
+++

# ChatGPT is Confused about Common Lisp

The following is a transcript of a "conversation" I had with ChatGPT last week
about Common Lisp.

> **ME**: Can you name a list of web sites that are powered by common lisp?
> 
> **ChatGPT**: Here is a list of websites powered by Common Lisp:
> 
>   - Reddit
>   - GIMP
>   - Maxima
>   - ASDF
>   - ABCL
>   - CLISP
>   - SBCL
>   - CL-HTTP
>   - CL-PPCRE
>   - CL-Markdown
> 
> Note: This list is not exhaustive, and some websites may use Common Lisp as part of their technology stack but not exclusively.
> 
> **ME**: That list is not correct.
> 
> **ChatGPT**: I apologize if the information provided was not accurate. Currently, there is no widely-known list of websites that are powered exclusively by Common Lisp, as it is not as widely used as other programming languages such as JavaScript, Python, or Ruby. However, Common Lisp is a powerful and versatile language that is often used in scientific, mathematical, and artificial intelligence applications.
> 
> **ME**: Grammarly is actually a website powered by Common Lisp.
> 
> **ChatGPT**: I apologize for the oversight. Yes, Grammarly is indeed a website that uses Common Lisp as part of its technology stack. Thank you for bringing this to my attention.
> 
> **ME**: Do you know of any others?
> 
> **ChatGPT**: Unfortunately, I don't have a comprehensive list of websites powered by Common Lisp, as it is not as widely used as other programming languages such as JavaScript, Python, or Ruby. However, Common Lisp is a powerful and versatile language that is often used in scientific, mathematical, and artificial intelligence applications. Additionally, it is also used in some niche applications, such as game development and system administration.
> 
> **ME**: (*confused*) How is Common Lisp used in System Administration?
> 
> **ChatGPT**: Common Lisp can be used in system administration tasks, such as automation of routine tasks, configuration management, and system monitoring.
> 
> Some common use cases include:
> 
>   - Automating system tasks such as file management, backup and restore operations, network communication, and more.
>   - Configuration management, where Common Lisp can be used to programmatically manage the configuration of servers and other systems.
>   - System monitoring, where Common Lisp can be used to gather and analyze data about system performance and usage.
> 
> In general, Common Lisp's ability to handle complex data structures, interact with other systems and applications, and perform automated tasks makes it a useful tool for system administrators. However, its use in this area is still relatively niche, and other languages such as Python and Ruby are more commonly used for system administration tasks.
> 
> **ME**: I have never heard of Common Lisp being used for Configuration Management [I am a DevOps Engineer, I use a lot fo Configuration Management tools, this isn't one I've used, unfortunately.]
> 
> **ChatGPT**: My apologies, you are correct. Common Lisp is not commonly used for configuration management. This is a specialized area where other languages such as Python, Ruby, or Puppet are more commonly used. Common Lisp is more often used in scientific, mathematical, and artificial intelligence applications, as well as some niche areas such as game development.