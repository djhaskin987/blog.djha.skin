+++
title = "Persistent pushd and popd"
date = 2022-12-12
tags = ["tooling", "shell"]
categories = ["Tooling","Shell"]
+++

```
2022-11-29T12:57:50-0700
```

## Overview

I use tmux on Linux and ConEMU on Windows. Both tools are some kind of terminal
multiplexer. I often create a new terminal window, but the window just
sends me back to my home directory. There are some ways around this, but I find
this to be so annoying that I just created my own set of tools around it.

## Method

To solve this problem, I write my "working directory" to a file. This makes a
lot of sense for me. When I want to work in a directory, I often wish to do
this across reboots. Then when any new shell is created, it changes directory
to that working directory. I wrote other tools that allow me to push and pop
directories to a text file, allowing me to treat this directory store not just
as a slot, but as a stack.

The tools are as follows:

- `ppushd <dir>`: Push directory `<dir>` to the persistent directory store, and
  change directories to it.
- `ppopd`: Pop a directory off of the directory store, and change directories
  to it.
- `pchanged`: Change directories to the current working directory.
- `plistd`: List the contents of the persistent directory stack.

Finally, I put a `pchanged` call at the end of my init shell script (`$profile`
for PowerShell, `~/.zshrc` for Mac and Linux).

Currently, [I only have this implemented for
windows](https://git.djha.skin/me/dotfiles/src/branch/main/Windows/Executables),
but plan on using the tools on Linux as well in the future.

## Conclusion

The last idea -- putting `pchanged` at the end of the init script -- feels
dirty, but it's changed my workflow for the better. I context switch a lot less
now.

Hopefully this will prove useful to someone else out there as well :)