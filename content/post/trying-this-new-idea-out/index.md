+++
title = "Trying this new idea out."
date = 2022-09-24
+++

I'm 35 years old. I am trying a new way to work online. New username, place to put my code, etc.

I have some old ideas, and some new ones. Hopefully they'll end up on this blog soon.

I'm thinking like more frequent posts, a paragraph or two each. I don't have the attention span for longer posts, being more of an extrovert by nature.

We'll see how it goes.