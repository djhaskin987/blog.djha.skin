+++
title = "How I Do the Pomodoro Technique, Part I: To-Do Today"
date = 2022-11-14
tags = ["pomodoro", "planning", "tooling"]
categories = ["Pomodoro"]
+++

# How I Do The Pomodoro Technique, Part One: To-Do Today

This month, my posts are focusing on my productivity tools.

*I have used the [Pomodoro
Technique](https://francescocirillo.com/products/the-pomodoro-technique#)
on-and-off for ten years. I find it really helps me keep track of what I'm
doing, especially since I have ADHD. Over time, patterns have emerged around
how I do the Pomodoro technique. This series showcases some of those things.*

## Planning

The technique specifies that you should first plan your day. I have turned this
into something of a ritual. Every day, I create a markdown file for
my "To Do Today" record and name it the ISO date for today's date, e.g.
`2022-10-22.md`. That way it's easy to find and sorts well.

I put a heading at the top of the file with today's date and day:

```

# 2022-10-12 Saturday

```

Then I paste in this content from my "To Do Today Template" file:


```

- [ ] Organization, 1:
  - [ ] Update previous day's diary entry.
  - [ ] Update [Pomodoro Spreadsheet](https://docs.google.com/spreadsheets/d/1V4D9Pb_3ZPIj6RTEwKRs7wtXcAuvxFSEHSRJmnd4kFo/edit)
  - [ ] Quick Communication, e.g. set appointments, follow-up
    - [ ] Check Chat Clients (Slack, Teams, etc).
    - [ ] Check Email & Calendar, load items into "Communication"
    - [ ] Review any other relevant "in" buckets, e.g., ticketing systems, pull
          requests, etc. Update them and put longer items into "Communication"
  - [ ] Write down Ideas into Activity Inventory and groom it
  - [ ] Compose To-Do Today
  - [ ] Stretch: Articles / Tooling
- [ ] Discretionary, (1):
- [ ] Communication, 1:
- [ ] Appointments
  - [ ] Stand-Up
- [ ] Commitments
- [ ] Unplanned/Urgent

```

Let's break the above down.

The first item on the list is the "Organization" pomodoro. It is the first
pomodoro of the day and is the time that I use to plan my day. It is estimated
to be 1 pomodoro in length (the `1` at the end of the line). I make my marks
after the colon for the task, a `,` for external interruptions, a `.` for
internal interruptions, and an `X` for a full pomodoro completed. I add
another, non-canonical mark: The *withdrawl failure*, the inability to stop
what I'm doing for a pomodor break. I make this mark if, after the five minute
break, I'm still working on whatever I'm working on. This mark is the hyphen,
`-`.

The second item is the "Discretionary" pomodoro. Its constrained to be one
pomodoro in length. That is what the parentheses around the `1` mean: Don't go
longer. This is how I record and do "open-ended" tasks. Open-ended tasks are
called "Spikes" in the scrum world. The idea is that instead of figuring out
how long it will take to do something, you allot yourself a specific amount of
time to explore and figure out what you're even doing and what needs done in
the first place. In this case, discretionary time is sort of like my 10% time.
I do whatever I want, as long as it's vaguely work related. I have often
gone over that 1-pomodoro limit, but if I don't schedule it at all, I find
I often go of-script more often. I need that time to "stay sane".

The third item is the "Communication" pomodoro. This is sort of my "office"
time, when I try to get all or most of my communication done. I make phone
calls, chat people, respond to longer emails.

The fourth item is the "Commitments" section. In this section, I put
records for pomodoros that are about me fulfilling a commitment, whether to
myself or others. They are the meat and potatoes of what needs to get done,
the reason why I do what I do.

I have two other sections, "Appointments" and "Unplanned/Urgent", where I
largely record activities that happened during the work day not captured by
Pomodoro. I do record things in "Unplanned/Urgent" under pomodoro if possible,
but far more often I record emergencies taht just came up.

Here is a sample day, at the end of it:

```

- [x] Organization, 1: .X
  - [x] Update previous day's diary entry.
  - [x] Update Pomodoro Spreadsheet
  - [x] Quick Communication, e.g. set appointments, follow-up
    - [x] Check Chat Clients (Slack, Teams, etc).
    - [x] Check Email & Calendar, load items into "Communication"
    - [x] Review any other relevant "in" buckets, e.g., ticketing systems, pull
          requests, etc. Update them and put longer items into "Communication"
  - [x] Write down Ideas into Activity Inventory and groom it
  - [x] Compose To-Do Today
  - [ ] ~~Stretch: Articles / Tooling~~
- [x] Discretionary, (1): X-
  - Try to get links in vim markdown working
- [x] Communication, 1: X
  - Reschedule doctor's appointment
  - Call plumber for that problem that came up at the house
  - DEVOPS-16 Coordinate with Tom about this
- [x] Appointments
  - [x] Stand-Up
  - [x] Pair-Program with Mel
- [x] Commitments
  - [x] DEVOPS-18 Clean up old VMs, 1: ,,
  - [x] DEVOPS-38 Set up reverse proxy initial spike, (2): X
  - [ ] Stretch: DEVOPS-17 Fix bug with DX CLI, 3: X,,
- [ ] Unplanned/Urgent
  - [x] Pair-Program with Rob on making Graphs
  - [x] Pair-Program with Shelly on setting up kubernetes monitoring

```

In the above example:
- I completed organization in one pomodoro, despite an internal interruption.
- I got distracted trying to get vim to work because I was frustrated that I
  didn't make headway in just 1 pomodoro on my vim configuration.
- I got all my communication done.
- I had two appointments, and took care of them.
- I had two other ad-hoc appointments that "came up" and took care of them.
- I kept gettting interrupted in the afternoon working on commitments, but
  somehow kept going, recording 0 "complete" pomodoros for DEVOPS-18
  but nevertheless finishing the task.
- I provide a "Stretch" task as part of commitments, showing what I will try
  to accomplish if what I said I would work on all gets done. In this case,
  I was able to start working on my stretch task but not finishing it.

It should be noted that the above example is an example of a *basically perfect*
day. It rarely goes so well. "The next pomodoro will go better."

In the next post, I will talk about how I organize my Activity Inventory.
Finally, I will talk about how I organize my Pomodoro Reports and Spreadsheets.