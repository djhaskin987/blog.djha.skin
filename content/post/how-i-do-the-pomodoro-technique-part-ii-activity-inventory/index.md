+++
title = "How I Do the Pomodoro Technique, Part II: Activity Inventory"
date = 2022-11-21
tags = ["tooling"]
categories = ["Pomodoro"]
+++

This month, my posts are focusing on my productivity tools.

*I have used the Pomodoro Technique on-and-off for ten years. I find it really
helps me keep track of what I'm doing, especially since I have ADHD. Over time,
patterns have emerged around how I do the Pomodoro Technique. This series
showcases some of those things.*

## Overview

I organize my activity inventory with 5 sections. These sections were largely
taken from the "Getting Things Done" discipline, but tweaked by myself according
to what I found useful and what I didn't:

- IN
- Commitments
- Communication
- R&D
- Delayed
- Discretionary
- Tooling

The part of organization called "Check ctivity Inventory & Groom it" in my
[previous
post](https://blog.djha.skin/blog/how-i-do-the-pomodoro-technique-part-i-to-do-today/)
refers to cleaning this up. When I groom the Activity Inventory, I put the
stuff from "IN" into the appropriate section. I also delete or edit things that
are still in those sections.


## IN

Any time I come up with a task to do, I put it in "IN".

## Commitments

This section contains commitments to do for myself or others. It is typically
pretty empty since if I really need to do something for someone else I usually
open up a ticket for it in the ticketing system. Still, things not in a
ticketing system land here. Often I'll add something to this section during a
meeting as an action item I've taken on to do.

## Communication

This section comprises tasks that are largely about talking to people or
following up with them. This includes support tickets that need shepherding,
"that one thing" Bob said he'd get you two weeks ago, calling the plumber, etc.

The "Communication" Section is particularly important. Every day when I do my
Communication pomodoro (see the previous post), I will simply highlight what's
in this section and place it in my communication pomodoro. Often, I will bug or
ping someone on an issue, receive no response, and leave their item in my
Activity Inventory until the issue is resolved, to keep it top of mind.

It also includes communication activities from home, not just work. I've found
that to be the best way to remember and follow up with people about stuff:
Just do it all at once.

## R&D

This section contains items that no one is asking for, but that I think if we
looked into them they could really help the organization. A new technology
that might really solve our problems, a proof of concept that I think needs
done. I will keep thinking about something as long as it lives on my list,
and will either convince the boss it should be done, in which case it graduates
to "Commitment", or else just do research it myself during my discretionary
time (see previous post).

## Delayed

This section comprises tasks that need done, but later. They have a date
associated with them for when I need to check back. After that date,
the task gets put back into whatever section it originally came from (most
often, the "Communication" section).

## Discretionary

This section comprises things that I want to do on my own authority. I do them
most often during my "discretionary" pomdooro.

## Tooling

This section comprises things I want to do to improve my tooling. They are
important to my job, but not all the time. It depends on how severly the
tooling deficiency affects my workflow. They are low priority to others, but
often high priority to me. Finding the time to fix these is usually a balancing
act.

## Example

Here is an example of the `ActivityInventory.md` file before and after it's
been groomed. All names below and circumstances are ficticious and meant to be
illustratitive.

Before:

```
# Activity Inventory

- IN
  - [ ] New pom column: Unplanned Meetings
  - [ ] research [some-vim-thing](https://github.com/falafel/some-vim-thing)
  - [ ] vim research g:clipboard
  - [ ] Monitoring stack is down down
  - [ ] Watch training video by Tuesday
  - [ ] When will that maintenance window happen?
- Commitments
  - [ ] Fix feature requests for Angelica
  - [ ] DEVOPS-50 !!! Ask Jordan about some-microservice!
- Communication
  - [ ] Kelly: I have unsaved changes in my git repo, do you still need them?
  - [ ] Follow-Up: Sawyer: Do we have an appointment scheduled yet?
  - [ ] Schedule the septic tank guy to come out
- R&D
  - [ ] Multi-repo CLI tool story using [gita](https://github.com/nosarthur/gita).
    - [ ] Presentation on the topic
      - [ ] In particular, a particular format.
      - [ ] Gita: standardize folder across all the apps for better management
      - [ ] How it could help e.g. buttercream team, shale team, etc.
- Discretionary
  - [ ] Clean my desk
- Delayed
  - 2022-12-16: Revisit 3rd party dependency audits
- Tooling
  - [ ] Laptop VPN tooling should be wrapped using systemctl
  - [ ] Vim markdown: `<Leader>f` should work with relative paths.

```

After:

```
# Activity Inventory

- IN
- Commitments
  - [ ] !! Monitoring stack is down down
  - [ ] Fix feature requests for Angelica
  - [ ] DEVOPS-50 !!! Ask Jordan about some-microservice!
- Communication
  - [ ] Watch training video
  - [ ] Jesse: When will that maintenance window happen?
  - [ ] Kelly: I have unsaved changes in my git repo, do you still need them?
- R&D
  - [ ] Multi-repo CLI tool story using [gita](https://github.com/nosarthur/gita).
    - [ ] Presentation on the topic
      - [ ] In particular, a particular format.
      - [ ] Gita: standardize folder across all the apps for better management
      - [ ] How it could help e.g. buttercream team, shale team, etc.
- Discretionary
  - [ ] Research [some-vim-thing](https://github.com/falafel/some-vim-thing)
- Delayed
  - 2022-12-16: Revisit 3rd party dependency audits
- Tooling
  - [ ] vim research g:clipboard
  - [ ] New pom column: Unplanned Meetings
  - [ ] Laptop VPN tooling should be wrapped using systemctl
  - [ ] Vim markdown: `<Leader>f` should work with relative paths.

```

As you can see, I categorized new activities, deleted old ones, and edited
tasks for grammar and accuracy.

## Keeping things straight

I try to do all this quickly. The reason for planning is to make the most
of our time, but it is important.

Next week will be a post about my Pomdoro Spreadsheets.