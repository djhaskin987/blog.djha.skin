+++
title = "I Love Paper Books"
date = 2023-02-21
tags = ["communication"]
categories = []
+++

I recently learned that [Barnes and Noble is doing well
again.](https://tedgioia.substack.com/p/what-can-we-learn-from-barnes-and)

I used to love going to Barnes & Noble as a teenager on my lunch breaks. I
worked as a lifeguard in downtown Naperville, Illinois. I would go there to read
or to find a book. Being surrounded by all those books gave me comfort. I'm
glad they're doing well again.

People I know often don't like paper books. They'll buy kindle over the real
book get the PDF or even take pictures of a page. I myself have done so. It is
much easier to read books on devices, and I get the desire for convenience.

However, I always prefer to buy my books (usually about programming or
technology) in print, even if they're available for free online as PDF. One
reason is that I want to pay the author for their work. Another is that I find
reading paper books more comfortable, despite their bulk. But the real major
reason for me and the reason I get something out of it is this: *print books
are much easier to share with others*.

If you want to share a book it's hard to do so when it's digital (unless it
truly is freely available). But even if it's freely available it just feels so
much better to hand someone a book and ask them to read it. "Working
Effectively with Legacy Code", "the Pomodoro Technique", "Design Patterns",
these are all books I've borrowed from others or loaned out myself.

Finally, books are easily readable by children. Paper books have had such a
powerful influence in my life, starting from a young age. The most influential
book in my life computer wise is easily "How to Solve it by Computer", and I
read parts of it when I was young. Nowadays kids that age might have cell
phones, or they might not, depending on how old they are, the preferences of
their parents, etc. Paper books are books that I can share with my children
*right now*. So many things children cannot experience yet because they are not
online yet.

For these reasons, I collect paper books. I have Scriptures, knitting books,
survival guides, and fiction. In particular, though, I collect paper tech
books. When I really want to learn something, books are the best way for me.
Paper books.

Here are some of the computer and productivity books that I am pleased in my
collection, in order of their influence on me:

 - [How to Solve it by Computer](https://www.thriftbooks.com/w/how-to-solve-it-by-computer-prentice-hall-international-series-in-computer-science_r-g-dromey/502194/?resultid=6368c318-0f2f-4846-8b4e-dba4658f46d0#isbn=0134339959&edition=1283942) by R.G. Dromey
 - [The Pomodoro Technique](https://www.thriftbooks.com/w/la-tecnica-del-pomodoro_francesco-cirillo/9537220/?resultid=1e075258-ef36-429d-b39e-d3ad43e02c72#edition=18475437&idiq=28840733) by Francesco Cirillo
 - [Working Effectively with Legacy Code](https://www.thriftbooks.com/w/working-effectively-with-legacy-code_michael-c-feathers/294013/?resultid=e007df22-c29b-468b-b454-1541a845cb29#isbn=0131177052) by Michael C. Feathers
 - [Common Lisp: The Language](https://www.thriftbooks.com/w/common-lisp-the-language_guy-l-steele-jr/342986/?resultid=995f37e8-cd72-48ea-8108-e380f8d4a48c#isbn=093237641) by Guy Steele Jr. (2nd Edition)
 - [Purely Functional Data Structures](https://www.thriftbooks.com/w/purely-functional-data-structures_chris-okasaki/648821/?resultid=c386ac10-9e23-445c-a4de-80aa59f6808e#isbn=0521663504) by Chris Okasaki
 - [The Relational Database the Relational Model For Matabase Management Version 2](https://www.thriftbooks.com/w/the-relational-model-for-database-management-version-2_e-f-codd/348983/?resultid=0fc8e56d-ac30-44e6-8702-e226ab6c6e6e#isbn=0201141922) by E.F. Codd
 - [Design Patterns](https://www.thriftbooks.com/w/design-patterns-elements-of-reusable-object-oriented-software-addison-wesley-professional-computing-series_erich-gamma/248223/?resultid=b0e90aee-e18c-462a-a0a6-8ea69fdf034c#isbn=0201633612) by Erich Gamma, Richard Helm, Ralph Johnson, and John Vlissides
 - [The Go Programming Language](https://www.thriftbooks.com/w/the-go-programming-language_brian-w-kernighan_alan-a-a-donovan/13867697/?resultid=da706592-8445-4267-8f03-9558d884c42f#isbn=0134190440) by Brian W. Kernighan and Alan A. A. Donova
 - [Pearls of Functional Algorithm Design](https://www.thriftbooks.com/w/pearls-of-functional-algorithm-design_richard-s-bird/10959618/?resultid=ebad7088-7d71-43ca-afad-73c6c05bcc15#isbn=0521513383) by Richard S. Bird
 - [Clojure Programming](https://www.thriftbooks.com/w/clojure-programming_brian-carper_christophe-grand/8979565/?resultid=a5098bb5-a128-4ebd-abc1-2956ec264a99#isbn=1449394701) by Chass Emerick, Brian Carper and Christophe Grand

There's something magical about turning information into a physical object.
Paper has some serious advantages. It doesn't run out of battery. It's not DRM
locked, but doesn't need to be because printing on paper is expensive enough
that it usually prevents pirating. It's very easy to use, even for the
technically illiterate.

The next time you want to educate your team about a new language or share some
concept with your friends, buy a paper book.