---
title: About
description: Why this blog?
date: '2022-11-10'
aliases:
  - about-us
  - about-me
  - contact
license: CC BY-NC-ND
lastmod: '2022-11-10'
menu:
    main: 
        weight: -90
        params:
            icon: user
---

See my [about page](https://about.djha.skin) for more information.

I have challenged myself to make this here blog and post to it for a year.
Wish me luck.
