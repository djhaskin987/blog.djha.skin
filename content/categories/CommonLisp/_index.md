---
title: "CommonLisp"
description: "Posts about Common Lisp and my journey to learn and use it."
slug: "common-lisp"
#image: "holiday-hoobie-whatie.jpg"
style:
    background: "#2a9d8f"
    color: "#fff"
---